################################################################################6
# Votre travail consiste à compléter les fonctions ci-dessous afin de          #
#  contribuer à la détection d'une victoire                                    #
################################################################################
# Il s'agit bien sûr de respecter scrupuleusement les specifications.
# Un commentaire pour les lignes de code non triviales est exigé.
# Pour tester votre script, il suffit d'exécuter le programme JeuPuissance4.
# Le jeu se joue avec la souris uniquement, les joueurs se la passe à tour de role.
# Si vos fonctions sont justes alors le jeu fonctionnera !
# Bon courage.

def alignement_horizontal(grille):
    ''' Cette fonction renvoie True si 4 pions de même couleur sont alignés
        horizontalement. Elle renvoie False sinon.   '''

    for i in range(len(grille)):
            for j in range(7):
                if grille[i][j] !=0:
                    if j <4 :
                        if grille[i][j]==grille[i][j+1]==grille[i][j+2]==grille[i][j+3] :
                            return True
            
    return False 
                            
def alignement_vertical(grille):
    ''' Cette fonction renvoie True si 4 pions de même couleur sont alignés
        horizontalement. Elle renvoie False sinon.   '''
    for i in range(len(grille)):
                for j in range(6):
                    if grille[i][j] !=0:
                        if i <3 :
                            if grille[i][j]==grille[i+1][j]==grille[i+2][j]==grille[i+3][j]: 
                                return True
        
    return False 

def alignement_quadrillage(grille):
    
    ''' Cette fonction renvoie True si la grille contient un alignement
        horizontal ou vertical de 4 pions de même couleur.
        Sinon elle renvoie False. '''
    return alignement_horizontal(grille) or alignement_vertical(grille) 